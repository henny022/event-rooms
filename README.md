# Event Rooms
a simple website for events to get people looking for rooms in contact with people looking for roommates

## Introduction
This Website is meant to help event attendees to find rooms to stay.

It allows users that have a room with free space to put that information out there.
Users that don't have a room can do the same.
Then users can use the contact information provided to come to an agreement in person so that hopefully everyone can get a room to stay.

## How to use

### Setup
At the moment when setting up a new database, you need to manually add the "Moderator" and "Admin" groups (case sensitive).
You can do this at the django admin page. They need no permissions.

#### Moderator
They can create events and manage events that they created.

#### Admin
They can manage any event.

### add a person
A user can manage multiple persons, so if you are traveling as a group, only one person needs to register on the website.
Because of that before doing anything you need to add at least one person.
Go to the Profile dropdown and select "Create Person". Enter a name and optionally some contact information.
This contact information can be used by other people to contact you about rooms or stuff.

### add a room
Go to the event, select the hotel your room is at.
There is a button to create a new room.
Select a person as owner, that is the person who is to be contacted about joining, and the max number of people you want in the room.
Click create and done.

### search for a room
Go to the event and create a Room Search for the event you are looking for a room for.

### join a room
After you created s room search, you can go through the available rooms at the event.
If you find one you want to join, you can use the "Request join" button.
The room owner will get an email notifying him about your request.
If he accepts, the person from the search will be added to the room and the search will be removed.

### invite person to your room
After you created a room, you can go through the people looking for a room and invite them.
The person will be notified via email, and if the invite is accepted, will join the room. 

## Docker image information
