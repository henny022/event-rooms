from main import models


def is_moderator(user):
    return user.groups.filter(name="Moderator").exists()


def is_admin(user):
    return user.groups.filter(name="Admin").exists()


def manages(user, thing):
    if isinstance(thing, models.Person):
        return thing.user == user
