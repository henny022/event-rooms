from django import template

from .. import account_util

register = template.Library()


@register.filter
def is_moderator(user):
    return account_util.is_moderator(user)


@register.filter
def is_admin(user):
    return account_util.is_moderator(user)


@register.filter
def manages(user, thing):
    return account_util.manages(user, thing)
