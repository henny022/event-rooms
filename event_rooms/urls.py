"""event_rooms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import DetailView

from main import models
from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('account.urls')),
    path('account/', include('django.contrib.auth.urls')),
    path('', views.index, name='index'),
    path('rooms/', views.FilterListView.as_view(model=models.Room), name='room_list'),
    path('room/<int:pk>/', DetailView.as_view(model=models.Room), name='room_detail'),
    path('events/', views.FilterListView.as_view(model=models.Event), name='event_list'),
    path('event/<int:pk>/', DetailView.as_view(model=models.Event), name='event_detail'),
    path('hotels/', views.FilterListView.as_view(model=models.Hotel), name='hotel_list'),
    path('hotel/<int:pk>/', DetailView.as_view(model=models.Hotel), name='hotel_detail'),
    path('persons/', views.FilterListView.as_view(model=models.Person), name='person_list'),
    path('person/<int:pk>/', DetailView.as_view(model=models.Person), name='person_detail'),
    path('searches/', views.FilterListView.as_view(model=models.RoomSearch), name='roomsearch_list'),
    path('search/<int:pk>/', DetailView.as_view(model=models.RoomSearch), name='roomsearch_detail'),
    path('invite/<int:room>/<int:search>/', views.create_invite, name='invite_create'),
    path('invite/<str:key>/', views.accept_invite, name='invite_accept'),
    path('request/<int:room>/<int:search>/', views.create_request, name='request_create'),
    path('request/<str:key>/', views.accept_request, name='request_accept'),
    path('create/room/<int:hotel_id>/', views.CreateRoomView.as_view(), name='room_create'),
    path('create/roomsearch/<int:event_id>/', views.CreateRoomSearchView.as_view(), name='roomsearch_create'),
    path('create/event/', views.CreateEventView.as_view(), name='event_create'),
    path('create/hotel/<int:event_id>/', views.CreateHotelView.as_view(), name='hotel_create'),
    path('create/person/', views.CreatePersonView.as_view(), name='person_create'),
    path('edit/person/<int:person_id>/', views.EditPersonView.as_view(), name='person_edit'),
    path('discord/', views.discord, name='discord'),
    path('template/<path:path>', views.debug, name='debug'),
]
