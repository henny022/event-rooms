from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.Event)
admin.site.register(models.Hotel)
admin.site.register(models.Room)
admin.site.register(models.Person)
admin.site.register(models.RoomSearch)
admin.site.register(models.RoomInvite)
admin.site.register(models.RoomRequest)
