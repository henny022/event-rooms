from django.forms import ModelForm, widgets

from . import models


class RoomSearchCreationForm(ModelForm):
    class Meta:
        model = models.RoomSearch
        fields = ['event', 'person', 'description']


class RoomCreationForm(ModelForm):
    class Meta:
        model = models.Room
        fields = ['hotel', 'owner', 'maxPeople', 'description']


class EventCreationForm(ModelForm):
    class Meta:
        model = models.Event
        fields = ['name']
        widgets = {'name': widgets.TextInput}


class HotelCreationForm(ModelForm):
    class Meta:
        model = models.Hotel
        fields = ['name', 'event']
        widgets = {'name': widgets.TextInput}


class PersonForm(ModelForm):
    class Meta:
        model = models.Person
        fields = ['name', 'email', 'discord', 'description']
        widgets = {'name': widgets.TextInput, 'email': widgets.TextInput, 'discord': widgets.TextInput}
