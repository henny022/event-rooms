from django.db import models
from django.contrib.auth.models import User
import hashlib
import base64

# Create your models here.


'''
@startuml
class Event
{
    +name: string
}

class Hotel
{
    +name: string
    +event: Event
}

class Room
{
    +hotel: Hotel
    +owner: Person
    +people: Person[]
    +maxPeople: int
    +description: string
    -preferredGender: Gender
}

class Person
{
    +name: string
    -gender: Gender
    +email: string
    +discord: string
    +description: string
    +user: User
}

class RoomSearch
{
    +event: Event
    +person: Person
    +description: string
}

RoomSearch --> Event
RoomSearch --> Person


enum Gender
{
    MALE
    FEMALE
    OTHER
    ANY
}



Hotel --> Event
Room --> Hotel
Room --> Person

Room --> Gender
Person --> Gender
@enduml
'''


class Event(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class Hotel(models.Model):
    name = models.TextField()
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.event} {self.name}"


class Person(models.Model):
    name = models.TextField()
    email = models.TextField(default='', blank=True)
    discord = models.TextField(default='', blank=True)
    description = models.TextField(default='', blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Room(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    owner = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='owned_rooms')
    people = models.ManyToManyField(Person, related_name='rooms')
    maxPeople = models.IntegerField()
    description = models.TextField(default='', blank=True)

    def _get_is_full(self):
        return self.people.count() >= self.maxPeople

    is_full = property(_get_is_full)

    def __str__(self):
        return f"{self.owner.name}'s room at {self.hotel}"


class RoomSearch(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    description = models.TextField(default='', blank=True)

    def __str__(self):
        return f"Room Search from {self.person} for {self.event}"


class RoomInvite(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    search = models.ForeignKey(RoomSearch, on_delete=models.CASCADE)
    _key = models.TextField(unique=True)

    def get_key(self):
        m = hashlib.sha256()
        m.update(bytes(self.id))
        key = base64.urlsafe_b64encode(m.digest()).decode('utf-8')
        self._key = key
        self.save()
        return key

    key = property(get_key)

    def __str__(self):
        return f"Invite {self.search.person} to {self.room}"


class RoomRequest(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    search = models.ForeignKey(RoomSearch, on_delete=models.CASCADE)
    _key = models.TextField(unique=True)

    def get_key(self):
        m = hashlib.sha256()
        m.update(bytes(self.id))
        key = base64.urlsafe_b64encode(m.digest()).decode('utf-8')
        self._key = key
        self.save()
        return key

    key = property(get_key)

    def __str__(self):
        return f"Request {self.search.person} to join {self.room}"
