from django import template
from django.utils.safestring import mark_safe

from widget_tweaks.templatetags import widget_tweaks

register = template.Library()


@register.filter
def floating_label(field):
    return mark_safe('<label for="%(id)s">%(name)s</label>' % {"id": field.id_for_label, "name": field.label})


@register.filter
def field_help(field):
    return mark_safe(
        '<small id="%(id)s_help" class="form-text text-muted">%(help)s</small>' % {"id": field.id_for_label,
                                                                                   "help": field.help_text})


@register.filter
def field_errors(field):
    return mark_safe(
        '<small id="%(id)s_errors" class="form-text text-danger">%(errors)s</small>' % {"id": field.id_for_label,
                                                                                        "errors": field.errors})


@register.filter
def floating_label_group(field):
    content = []
    content.append('<div class="floating-label-group">')
    content.append(
        str(widget_tweaks.add_class(widget_tweaks.set_attr(field, "placeholder:%s" % field.label), "form-control")))
    content.append(str(floating_label(field)))
    content.append(field_errors(field))
    content.append(field_help(field))
    content.append('</div>')
    return mark_safe("\n".join(content))


@register.filter
def floating_label_form(form):
    content = []
    for field in form:
        content.append(floating_label_group(field))
    return mark_safe('\n'.join(content))
