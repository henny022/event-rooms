import re

from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse
from django.views import View
from django.views.generic import ListView

from account.account_util import is_moderator, manages
from . import models, forms


def unauthorized():
    return HttpResponse("unauthorized", status=401)  # TODO render proper error page


# Create your views here.

def index(request):
    return render(request, 'index.html')


class FilterListView(ListView):
    def get_queryset(self):
        queryset = super().get_queryset()
        filter = {}
        for key in self.request.GET:
            key = str(key)
            # process key
            if key.startswith('Count__'):
                a, field, b = tuple(key.split('__'))
                kwargs = {f'{a}__{field}': models.models.Count(field)}
                queryset = queryset.annotate(**kwargs)
            # process value
            match = re.match(r'F\((?P<field>\w*)\)', self.request.GET[key])
            if match:
                field = match.group('field')
                filter[key] = models.models.F(field)
            else:
                filter[key] = self.request.GET[key]
        queryset = queryset.filter(**filter)
        return queryset


def create_invite(request, room, search):
    invite = models.RoomInvite.objects.create(room=models.Room.objects.get(id=room),
                                              search=models.RoomSearch.objects.get(id=search))
    # TODO replace with send mail
    return HttpResponse(invite.key)


def create_request(request, room, search):
    room_request = models.RoomRequest.objects.create(room=models.Room.objects.get(id=room),
                                                     search=models.RoomSearch.objects.get(id=search))
    # TODO replace with send mail
    return HttpResponse(room_request.key)


def accept_invite(request, key):
    invite = models.RoomInvite.objects.get(_key=key)
    room = invite.room
    search = invite.search
    room.people.add(search.person)
    room.save()
    search.delete()
    invite.delete()
    return redirect(reverse('room_detail', kwargs={'pk': room.pk}))


def accept_request(request, key):
    room_request = models.RoomRequest.objects.get(_key=key)
    room = room_request.room
    search = room_request.search
    room.people.add(search.person)
    room.save()
    search.delete()
    room_request.delete()
    return redirect(reverse('room_detail', kwargs={'pk': room.pk}))


class CreateRoomSearchView(View):
    def get(self, request, event_id):
        form = forms.RoomSearchCreationForm(initial={'event': event_id})
        form.fields['person'].queryset = models.Person.objects.filter(user=request.user)
        return render(request, 'main/create.html', {'form': form, 'submit': 'Create Room Search'})

    def post(self, request, event_id):
        form = forms.RoomSearchCreationForm(request.POST)
        search = form.save()
        return redirect(reverse('roomsearch_detail', kwargs={'pk': search.pk}))


class CreateRoomView(View):
    def get(self, request, hotel_id):
        form = forms.RoomCreationForm(initial={'hotel': hotel_id})
        form.fields['owner'].queryset = models.Person.objects.filter(user=request.user)
        return render(request, 'main/create.html', {'form': form, 'submit': 'Create Room'})

    def post(self, request, hotel_id):
        form = forms.RoomCreationForm(request.POST)
        room = form.save()
        room.people.add(room.owner)
        room.save()
        return redirect(reverse('room_detail', kwargs={'pk': room.pk}))


class CreateEventView(View):
    def get(self, request):
        if not is_moderator(request.user):
            return unauthorized()
        form = forms.EventCreationForm()
        return render(request, 'main/create.html', {'form': form, 'submit': 'Create Event'})

    def post(self, request):
        if not is_moderator(request.user):
            return unauthorized()
        form = forms.EventCreationForm(request.POST)
        event = form.save()
        return redirect(reverse('event_detail', kwargs={'pk': event.pk}))


class CreateHotelView(View):
    def get(self, request, event_id):
        if not is_moderator(request.user):
            return unauthorized()
        form = forms.HotelCreationForm(initial={'event': event_id})
        return render(request, 'main/create.html', {'form': form, 'submit': 'Create Hotel'})

    def post(self, request, event_id):
        if not is_moderator(request.user):
            return unauthorized()
        form = forms.HotelCreationForm(request.POST)
        hotel = form.save()
        return redirect(reverse('hotel_detail', kwargs={'pk': hotel.pk}))


class CreatePersonView(View):
    def get(self, request):
        form = forms.PersonForm()
        return render(request, 'main/create.html', {'form': form, 'submit': 'Create Person'})

    def post(self, request):
        form = forms.PersonForm(request.POST)
        person = form.save(commit=False)
        person.user = request.user
        person.save()
        return redirect(reverse('person_detail', kwargs={'pk': person.pk}))


class EditPersonView(View):
    def get(self, request, person_id):
        person = models.Person.objects.get(pk=person_id)
        if not manages(request.user, person):
            return unauthorized()
        form = forms.PersonForm(instance=person)
        return render(request, 'main/create.html', {'form': form, 'submit': 'Edit Person'})

    def post(self, request, person_id):
        person = models.Person.objects.get(pk=person_id)
        if not manages(request.user, person):
            return unauthorized()
        form = forms.PersonForm(request.POST, instance=person)
        form.save()
        return redirect(reverse('person_detail', kwargs={'pk': person.pk}))


redirect_url = 'https://discordapp.com/api/oauth2/authorize?client_id=656727286003007498&redirect_uri=http%3A%2F%2Flocalhost%3A8000%2Fdiscord&response_type=code&scope=identify'


def discord(request):
    if 'code' in request.GET:
        code = request.GET['code']
        return HttpResponse(code)
    return redirect(redirect_url)


def debug(request, path):
    return render(request, f'{path}.html')
